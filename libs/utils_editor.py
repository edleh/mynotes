# -*- coding: utf8 -*-

# -----------------------------------------------------------------
# This file is a part of MyNotes project.
# Name:         MyNotes: a little notes soft
# Copyright:    (C) 2015-2021 Pascal Peter
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    un QSyntaxHighlighter
    http://dvlabs.tippingpoint.com/blog/2012/02/26/mindshare-syntax-coloring
    https://github.com/rupeshk/MarkdownHighlighter

    pour gérer la correction orthographique :
    http://john.nachtimwald.com/2009/08/22/qplaintextedit-with-in-line-spell-check/
"""

# importation des modules utiles :
import re

import utils, utils_functions
from spellchecker import SpellChecker

from PyQt5 import QtCore, QtWidgets, QtGui




class markdownEditor(QtWidgets.QTextEdit):
    """
    un éditeur avec coloration syntaxique
    et reconnaissance des mots traduits.
    """
    def __init__(self, parent=None, readOnly=False):
        super(markdownEditor, self).__init__(parent)
        self.main = parent
        self.ctrl = False
        self.readOnly = readOnly
        if readOnly:
            self.setReadOnly(True)
        #self.setFontFamily('DejaVu Sans Mono')
        self.fontPointSize = 9
        self.setFontPointSize(self.fontPointSize)

        self.highlighter = MarkdownHighlighter(self)

        self.initDict()
        self.popup_menu = QtWidgets.QMenu(self)

    def initDict(self):
        # on teste d'abord avec locale (par exemple fr_FR),
        # puis avec lang (par exemple fr) :
        self.spellEnable = False
        language = 'en'
        path = 'libs/spellchecker/resources/{0}.json.gz'
        localeFileName = path.format(utils.LOCALE)
        if QtCore.QFileInfo(localeFileName).exists():
            language = utils.LOCALE
        else:
            lang = utils.LOCALE.split('_')[0]
            langFileName = path.format(lang)
            if QtCore.QFileInfo(langFileName).exists():
                language = lang
        self.spell = SpellChecker(language=language)
        if self.spell:
            self.highlighter.setDict(self.spell)
            self.highlighter.rehighlight()

    def setSpellCheck(self, value):
        self.spellEnable = value


    def keyPressEvent(self, event):
        if not(self.readOnly):
            if event.key() == QtCore.Qt.Key_Control:
                self.ctrl = True
            elif event.key() == QtCore.Qt.Key_Tab:
                self.textCursor().insertText('    ')
                return
        QtWidgets.QTextEdit.keyPressEvent(self, event)

    def keyReleaseEvent(self, event):
        if not(self.readOnly):
            if event.key() == QtCore.Qt.Key_Control:
                self.ctrl = False
        QtWidgets.QTextEdit.keyReleaseEvent(self, event)

    def wheelEvent(self, event):
        if not(self.readOnly) and self.ctrl:
            angle = event.angleDelta().y()
            mini = 6
            maxi = 40
            if angle > 0:
                self.fontPointSize += 1
            else:
                self.fontPointSize -= 1
            if self.fontPointSize > maxi:
                self.fontPointSize = maxi
            elif self.fontPointSize < mini:
                self.fontPointSize = mini
            self.setFontPointSize(self.fontPointSize)
            # pour esquiver l'appel à main.textChanged :
            self.main.noChange = True
            self.setPlainText(self.toPlainText())
        else:
            QtWidgets.QTextEdit.wheelEvent(self, event)



    def mousePressEvent(self, event):
        if self.spellEnable:
            if event.button() == QtCore.Qt.RightButton:
                # Rewrite the mouse event to a left button event so the cursor is
                # moved to the location of the pointer.
                event = QtGui.QMouseEvent(
                    QtCore.QEvent.MouseButtonPress, event.pos(),
                    QtCore.Qt.LeftButton, QtCore.Qt.LeftButton, QtCore.Qt.NoModifier)
        QtWidgets.QTextEdit.mousePressEvent(self, event)

    def contextMenuEvent(self, event):
        self.popup_menu.clear()
        self.popup_menu = self.createStandardContextMenu()
        first = self.popup_menu.actions()[0]

        if self.spellEnable:
            # Select the word under the cursor.
            cursor = self.textCursor()
            cursor.select(QtGui.QTextCursor.WordUnderCursor)
            self.setTextCursor(cursor)

            # Check if the selected word is misspelled and offer spelling
            # suggestions if it is.
            if self.textCursor().hasSelection():
                text = self.textCursor().selectedText()
                istitle = text.istitle()
                if len(self.spell.known([text,])) < 1:
                    for word in self.spell.candidates(text):
                        if istitle:
                            newAct = QtWidgets.QAction(word.title(), self)
                        else:
                            newAct = QtWidgets.QAction(word, self)
                        newAct.triggered.connect(self.menuSelected)
                        self.popup_menu.insertAction(first, newAct)
                self.popup_menu.insertSeparator(first)

        self.popup_menu.exec_(event.globalPos())

    def menuSelected(self):
        word = self.sender().text()
        self.correctWord(word)

    def correctWord(self, word):
        """
        Replaces the selected text with word.
        """
        cursor = self.textCursor()
        cursor.beginEditBlock()
        cursor.removeSelectedText()
        cursor.insertText(word)
        cursor.endEditBlock()





class MarkdownHighlighter(QtGui.QSyntaxHighlighter):
    """
    
    """

    MARKDOWN_KEYS_REGEX = {
        'Bold' : re.compile(u'(?P<delim>\*\*)(?P<text>.+)(?P=delim)'), 
        'uBold': re.compile(u'(?P<delim>__)(?P<text>[^_]{2,})(?P=delim)'), 
        'Italic': re.compile(u'(?P<delim>\*)(?P<text>[^*]{2,})(?P=delim)'), 
        'uItalic': re.compile(u'(?P<delim>_)(?P<text>[^_]+)(?P=delim)'), 
        'Link': re.compile(u'(?u)(^|(?P<pre>[^!]))\[.*?\]:?[ \t]*\(?[^)]+\)?'), 
        'Image': re.compile(u'(?u)!\[.*?\]\(.+?\)'), 
        'HeaderAtx': re.compile(u'(?u)^\#{1,6}(.*?)\#*(\n|$)'), 
        'Header': re.compile(u'^(.+)[ \t]*\n(=+|-+)[ \t]*\n+'), 
        'CodeBlock': re.compile(u'^([ ]{4,}|\t).*'), 
        'UnorderedList': re.compile(u'(?u)^\s*(\* |\+ |- )+\s*'), 
        'UnorderedListStar': re.compile(u'^\s*(\* )+\s*'), 
        'OrderedList': re.compile(u'(?u)^\s*(\d+\. )\s*'), 
        'BlockQuote': re.compile(u'(?u)^\s*>+\s*'), 
        'BlockQuoteCount': re.compile(u'^[ \t]*>[ \t]?'), 
        'CodeSpan': re.compile(u'(?P<delim>`+).+?(?P=delim)'), 
        'HR': re.compile(u'(?u)^(\s*(\*|-)\s*){3,}$'), 
        'eHR': re.compile(u'(?u)^(\s*(\*|=)\s*){3,}$'), 
        'Html': re.compile(u'<.+?>'), 
        }

    def __init__(self, parent):
        QtGui.QSyntaxHighlighter.__init__(self, parent)
        self.parent = parent
        self.spell = None
        self.regExp = re.compile('^[-+]?([0-9,.]*)$')

        self.defaultTheme =  {
            "background-color":"#ffffff", 
            "color":"#000000", 
            "bold": {"color":"#859900", "font-weight":"bold", "font-style":"normal"}, 
            "emphasis": {"color":"#b58900", "font-weight":"bold", "font-style":"italic"}, 
            "link": {"color":"#cb4b16", "font-weight":"normal", "font-style":"normal"}, 
            "image": {"color":"#cb4b16", "font-weight":"normal", "font-style":"normal"}, 
            "header": {"color":"#2aa198", "font-weight":"bold", "font-style":"normal"}, 
            "unorderedlist": {"color":"#dc322f", "font-weight":"normal", "font-style":"normal"}, 
            "orderedlist": {"color":"#dc322f", "font-weight":"normal", "font-style":"normal"}, 
            "blockquote": {"color":"#dc322f", "font-weight":"normal", "font-style":"normal"}, 
            "codespan": {"color":"#dc322f", "font-weight":"normal", "font-style":"normal"}, 
            "codeblock": {"color":"#ff9900", "font-weight":"normal", "font-style":"normal"}, 
            "line": {"color":"#2aa198", "font-weight":"normal", "font-style":"normal"}, 
            "html": {"color":"#c000c0", "font-weight":"normal", "font-style":"normal"}, 
            }
        self.setTheme(self.defaultTheme)

    def setTheme(self, theme):

        def doFormat(theme, what):
            newFormat = QtGui.QTextCharFormat()
            newFormat.setForeground(
                QtGui.QBrush(QtGui.QColor(theme[what]['color'])))
            newFormat.setFontWeight(
                QtGui.QFont.Bold 
                if theme[what]['font-weight'] == 'bold' 
                else QtGui.QFont.Normal)
            newFormat.setFontItalic(
                True if theme[what]['font-style'] == 'italic' else False)
            return newFormat

        self.theme = theme
        self.MARKDOWN_KWS_FORMAT = {}

        pal = self.parent.palette()
        pal.setColor(
            QtGui.QPalette.Base, QtGui.QColor(theme['background-color']))
        self.parent.setPalette(pal)
        self.parent.setTextColor(QtGui.QColor(theme['color']))

        self.MARKDOWN_KWS_FORMAT['Bold'] = doFormat(theme, 'bold')
        self.MARKDOWN_KWS_FORMAT['uBold'] = doFormat(theme, 'bold')
        self.MARKDOWN_KWS_FORMAT['Italic'] = doFormat(theme, 'emphasis')
        self.MARKDOWN_KWS_FORMAT['uItalic'] = doFormat(theme, 'emphasis')
        self.MARKDOWN_KWS_FORMAT['Link'] = doFormat(theme, 'link')
        self.MARKDOWN_KWS_FORMAT['Image'] = doFormat(theme, 'image')
        self.MARKDOWN_KWS_FORMAT['Header'] = doFormat(theme, 'header')
        self.MARKDOWN_KWS_FORMAT['HeaderAtx'] = doFormat(theme, 'header')
        self.MARKDOWN_KWS_FORMAT['UnorderedList'] = doFormat(
            theme, 'unorderedlist')
        self.MARKDOWN_KWS_FORMAT['OrderedList'] = doFormat(
            theme, 'orderedlist')
        self.MARKDOWN_KWS_FORMAT['BlockQuote'] = doFormat(theme, 'blockquote')
        self.MARKDOWN_KWS_FORMAT['CodeSpan'] = doFormat(theme, 'codespan')
        self.MARKDOWN_KWS_FORMAT['CodeBlock'] = doFormat(theme, 'codeblock')
        self.MARKDOWN_KWS_FORMAT['HR'] = doFormat(theme, 'line')
        self.MARKDOWN_KWS_FORMAT['eHR'] = doFormat(theme, 'line')
        self.MARKDOWN_KWS_FORMAT['HTML'] = doFormat(theme, 'html')

        self.rehighlight()

    def setDict(self, spell):
        self.spell = spell

    def highlightBlock(self, text):
        self.highlightMarkdown(text, 0)
        self.highlightHtml(text)

        if not self.spell:
            return
        elif not self.parent.spellEnable:
            return
        textFormat = QtGui.QTextCharFormat()
        textFormat.setUnderlineColor(QtCore.Qt.red)
        textFormat.setUnderlineStyle(QtGui.QTextCharFormat.WaveUnderline)
        for word_object in re.finditer(r'\w+', text, re.UNICODE):
            if self.regExp.match(word_object.group()) == None:
                if len(self.spell.known([word_object.group().lower(),])) < 1:
                    self.setFormat(word_object.start(),
                        word_object.end() - word_object.start(), textFormat)

    def highlightMarkdown(self, text, strt):
        cursor = QtGui.QTextCursor(self.document())
        bf = cursor.blockFormat()
        self.setFormat(0, len(text), QtGui.QColor(self.theme['color']))

        #Block quotes can contain all elements so process it first
        self.highlightBlockQuote(text, cursor, bf, strt)

        #If empty line no need to check for below elements just return
        if self.highlightEmptyLine(text, cursor, bf, strt):
            return

        #If horizontal line, look at pevious line to see 
        #if its a header, process and return
        if self.highlightHorizontalLine(text, cursor, bf, strt):
            return

        if self.highlightAtxHeader(text, cursor, bf, strt):
            return

        self.highlightList(text, cursor, bf, strt)
        self.highlightLink(text, cursor, bf, strt)
        self.highlightImage(text, cursor, bf, strt)
        self.highlightCodeSpan(text, cursor, bf, strt)
        self.highlightEmphasis(text, cursor, bf, strt)
        self.highlightBold(text, cursor, bf, strt)
        self.highlightCodeBlock(text, cursor, bf, strt)

    def highlightBlockQuote(self, text, cursor, bf, strt):
        found = False
        mo = re.search(self.MARKDOWN_KEYS_REGEX['BlockQuote'], text)
        if mo:
            self.setFormat(
                mo.start(), 
                mo.end() - mo.start(), 
                self.MARKDOWN_KWS_FORMAT['BlockQuote'])
            unquote = re.sub(
                self.MARKDOWN_KEYS_REGEX['BlockQuoteCount'], '', text)
            spcs = re.match(
                self.MARKDOWN_KEYS_REGEX['BlockQuoteCount'], text)
            spcslen = 0
            if spcs:
                spcslen = len(spcs.group(0))
            self.highlightMarkdown(unquote, spcslen)
            found = True
        return found

    def highlightEmptyLine(self, text, cursor, bf, strt):
        #textAscii = str(text.replace('\u2029','\n'))
        textAscii = text.replace(u'\u2029', '\n')
        if textAscii.strip():
            return False
        else:
            return True

    def highlightHorizontalLine(self, text, cursor, bf, strt):
        found = False
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['HR'], text):
            prevBlock = self.currentBlock().previous()
            prevCursor = QtGui.QTextCursor(prevBlock)
            prev = prevBlock.text()
            #prevAscii = str(prev.replace(u'\u2029','\n'))
            prevAscii = prev.replace(u'\u2029', '\n')
            if prevAscii.strip():
                #print "Its a header"
                prevCursor.select(QtGui.QTextCursor.LineUnderCursor)
                #prevCursor.setCharFormat(self.MARKDOWN_KWS_FORMAT['Header'])
                formatRange = QtGui.QTextLayout.FormatRange()
                formatRange.format = self.MARKDOWN_KWS_FORMAT['Header']
                formatRange.length = prevCursor.block().length()
                formatRange.start = 0
                prevCursor.block().layout().setAdditionalFormats([formatRange])
            self.setFormat(
                mo.start() + strt, 
                mo.end() - mo.start(), 
                self.MARKDOWN_KWS_FORMAT['HR'])

        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['eHR'], text):
            prevBlock = self.currentBlock().previous()
            prevCursor = QtGui.QTextCursor(prevBlock)
            prev = prevBlock.text()
            #prevAscii = str(prev.replace(u'\u2029','\n'))
            prevAscii = prev.replace(u'\u2029', '\n')
            if prevAscii.strip():
                #print "Its a header"
                prevCursor.select(QtGui.QTextCursor.LineUnderCursor)
                #prevCursor.setCharFormat(self.MARKDOWN_KWS_FORMAT['Header'])
                formatRange = QtGui.QTextLayout.FormatRange()
                formatRange.format = self.MARKDOWN_KWS_FORMAT['Header']
                formatRange.length = prevCursor.block().length()
                formatRange.start = 0
                prevCursor.block().layout().setAdditionalFormats([formatRange])
            self.setFormat(
                mo.start() + strt, 
                mo.end() - mo.start(), 
                self.MARKDOWN_KWS_FORMAT['HR'])
        return found

    def highlightAtxHeader(self, text, cursor, bf, strt):
        found = False
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['HeaderAtx'], text):
            self.setFormat(
                mo.start() + strt, 
                mo.end() - mo.start(), 
                self.MARKDOWN_KWS_FORMAT['HeaderAtx'])
            found = True
        return found














    def doHighlight(self, text, cursor, bf, strt, what):
        found = False
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX[what], text):
            self.setFormat(
                mo.start() + strt, 
                mo.end() - mo.start() - strt, 
                self.MARKDOWN_KWS_FORMAT[what])
            found = True
        return found

    def highlightList(self, text, cursor, bf, strt):
        found = False
        if self.doHighlight(text, cursor, bf, strt, 'UnorderedList'):
            found = True
        if self.doHighlight(text, cursor, bf, strt, 'OrderedList'):
            found = True
        return found

    def highlightLink(self, text, cursor, bf, strt):
        return self.doHighlight(text, cursor, bf, strt, 'Link')

    def highlightImage(self, text, cursor, bf, strt):
        return self.doHighlight(text, cursor, bf, strt, 'Image')

    def highlightCodeSpan(self, text, cursor, bf, strt):
        return self.doHighlight(text, cursor, bf, strt, 'CodeSpan')

    def highlightBold(self, text, cursor, bf, strt):
        found = False
        if self.doHighlight(text, cursor, bf, strt, 'Bold'):
            found = True
        if self.doHighlight(text, cursor, bf, strt, 'uBold'):
            found = True
        return found

    def highlightEmphasis(self, text, cursor, bf, strt):
        found = False
        unlist = re.sub(
            self.MARKDOWN_KEYS_REGEX['UnorderedListStar'], '', text)
        spcs = re.match(
            self.MARKDOWN_KEYS_REGEX['UnorderedListStar'], text)
        spcslen = 0
        if spcs:
            spcslen = len(spcs.group(0))
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['Italic'], unlist):
            self.setFormat(
                mo.start() + strt + spcslen, 
                mo.end() - mo.start() - strt, 
                self.MARKDOWN_KWS_FORMAT['Italic'])
            found = True
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['uItalic'], text):
            self.setFormat(
                mo.start() + strt, 
                mo.end() - mo.start() - strt, 
                self.MARKDOWN_KWS_FORMAT['uItalic'])
            found = True
        return found

    def highlightCodeBlock(self, text, cursor, bf, strt):
        found = False
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['CodeBlock'], text):
            stripped = text.lstrip()
            if stripped[0] not in ('*', '-', '+', '>'):
                self.setFormat(
                    mo.start() + strt, 
                    mo.end() - mo.start(), 
                    self.MARKDOWN_KWS_FORMAT['CodeBlock'])
                found = True
        return found

    def highlightHtml(self, text):
        for mo in re.finditer(self.MARKDOWN_KEYS_REGEX['Html'], text):
            self.setFormat(
                mo.start(), 
                mo.end() - mo.start(), 
                self.MARKDOWN_KWS_FORMAT['HTML'])




