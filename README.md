# MyNotes

* **Website:** http://pascal.peter.free.fr
* **Email:** pascal.peter at free.fr
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 2015-2021

----

#### REQUIREMENTS
* [Python](https://www.python.org): programming language
* [Qt](https://www.qt.io): "toolkit" comprehensive (graphical user interface and a lot of things)
* [PyQt](https://riverbankcomputing.com): link between Python and Qt

#### Other libraries used and other stuff
* [pyspellchecker](https://github.com/barrust/pyspellchecker): for spell Checker
* [marked](https://github.com/markedjs/marked): to view the Markdown files
* [Bootstrap](https://getbootstrap.com): framework CSS/JS
* [Breeze Icons](https://api.kde.org/frameworks/breeze-icons/html/index.html)
* [Blender](https://www.blender.org): for creating the logo

